# README

The parent group of this project (`hands-on`) allows you to host some quick hands-on for your customers, prospects, friends, ...

## How to initialize a hands-on

- Create a new sub-group in the `hands-on` (for example: `2020-12-25-acme`)
- Add the attendees as members of this new sub-group (🖐️ and not of the parent group) with the appropriate rights
- When the hands-on is started, ask to every attendee to create a new project **from template**
  - click on <kbd>New project</kbd> button
  - choose the **"Create from template"**
  - select the **Group** tab
  - choose the appropriate template by clicking on <kbd>Use template</kbd>
  - give a name to the project (🖐️ all project names must be different, for example the attendees can use a trigram or a nickname as suffix or prefix of the project name)
  - set the **Visibility level**
  - click on <kbd>Create project</kbd>
  - wait a moment for the end of the project import ⏳
- Enjoy your hands-on (go to the board of the project to discover what to do)
